//
//  Classroom.swift
//  ameeralimk
//
//  Created by ameerali on 23/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import Foundation
import Foundation
class Classroom:NSObject{
  
    var id :Int?
    var layout:String?
    var name:String?
    var size:Int?

    override init() {
        id = 0
        layout = ""
        name = ""
        size = 0
    }
    init(dic:NSDictionary){
        id = dic.value(forKey:"id") as? Int
        layout = dic.value(forKey:"layout") as? String
        name = dic.value(forKey:"name") as? String
        size = dic.value(forKey:"size") as? Int
    }
}
