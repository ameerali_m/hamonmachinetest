//
//  Students.swift
//  ameeralimk
//
//  Created by ameerali on 23/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import Foundation
class Students:NSObject{
    var age:String?
    var emaill:String?
    var id :Int?
    var name:String?
    
    override init() {
        age = ""
        emaill = ""
        id = 0
        name = ""
    }
    init(dic:NSDictionary){
        age = dic.value(forKey:"age") as? String
        emaill = dic.value(forKey:"email") as? String
        id = dic.value(forKey:"id") as? Int
        name = dic.value(forKey:"name") as? String
    }
}
