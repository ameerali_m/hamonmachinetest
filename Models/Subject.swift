//
//  Subject.swift
//  ameeralimk
//
//  Created by ameerali on 23/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import Foundation
import Foundation
class Subjects:NSObject{
    var credits:Int?
    var id:Int?
    var name :String?
    var teacher:String?
    
   
    
    override init() {
        credits = 0
        id = 0
        name = ""
        teacher = ""
    }
    init(dic:NSDictionary){
        credits = dic.value(forKey:"credits") as? Int
        id = dic.value(forKey:"id") as? Int
        name = dic.value(forKey:"name") as? String
        teacher = dic.value(forKey:"teacher") as? String
    }
}
