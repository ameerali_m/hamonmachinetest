//
//  ClassromDetailViewController.swift
//  ameeralimk
//
//  Created by ameerali on 24/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import UIKit
import iProgressHUD
import Alamofire
class ClassromDetailViewController: UIViewController {
    @IBOutlet weak var btnAssignStudents: UIButton!
    @IBOutlet weak var txtSubject: UITextField!
    
    var subjectID = Int()
    
    var subjects = [Subjects]()
    var id = Int()
     var picker = UIPickerView()
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblLayout:UILabel!
    @IBOutlet weak var lblSize:UILabel!


    override func viewDidLoad() {
        
        super.viewDidLoad()
        let iprogress: iProgressHUD = iProgressHUD()
        iprogress.isShowModal = false
        iprogress.boxSize =  25
        iprogress.boxColor = UIColor.darkGray
        
        //iprogress.captionColor = UIColor.white
        iprogress.indicatorColor = UIColor.white
        iprogress.isShowCaption = false
        iprogress.isTouchDismiss = true
        iprogress.indicatorStyle = .ballSpinFadeLoader
        iprogress.alphaModal = 1
        iprogress.attachProgress(toViews: view)
        picker.dataSource = self
        picker.delegate = self
getAllSubjects()
        getDetailByID()
        txtSubject.inputView = picker
        
    }
    
   
    func getAllSubjects(){
        ApiHelper.get(url: BGNetworkConstants.SUBJECTS) { (response) in
            
            switch(response.result){
            case .success(_):
                let res = response.result.value as? NSDictionary
                let data = res?.value(forKey: "subjects") as? NSArray
                let count = data?.count
                if count != nil{
                    for i in 0...count! - 1 {
                        let dic = data?.object(at: i) as? NSDictionary
                        self.subjects.append(Subjects.init(dic: dic!))
                    }
                }
                DispatchQueue.main.async {
                    print(self.subjects)
                    self.picker.reloadAllComponents()
                }
                break
            case .failure(let error):
                print(error)
                break
            }        }
    }
    @IBAction func assign(_ sender: Any) {
        if let subjectid = subjectID as? NSNumber{
            let url = "https://hamon-interviewapi.herokuapp.com/classrooms/\(id)?api_key=EbDd6"
            let subjeID = "subject=\(subjectid.stringValue)"
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept-Encoding": "gzip, deflate",
                ]
            let postData = NSMutableData(data:subjeID.data(using: String.Encoding.utf8)!)
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "PATCH"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    print(error)
                } else {
                    DispatchQueue.main.async {
                             Utils.showAlert(title: "Assign", message: "Assigned subject successfully", delegate: self)
                    }
                
                    let httpResponse = response as? HTTPURLResponse
                }
            })
            dataTask.resume()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? AssignStudentViewController{
            destination.Subid = subjectID
        }
    }
   
    @IBAction func gotoAssignStudent(_ sender: Any) {
        self.performSegue(withIdentifier: "assignStudent", sender: self)
    }
    func getDetailByID(){
        view.showProgress()
        
        let url = "https://hamon-interviewapi.herokuapp.com/classrooms/\(id)?api_key=EbDd6"
        ApiHelper.get(url: url) { (response) in
            switch(response.result){
            case.success(let data):
                print(data)
                if let content = data as? NSDictionary{
                    DispatchQueue.main.async {
                        if let subid = content.value(forKey: "subject") as? Int{
                            self.subjectID = subid
                        }
                        if let layout = content.value(forKey: "layout") as? String {
                            self.lblLayout.text = " layout is \(layout)"
                        }
                        if let name = content.value(forKey: "name") as? String{
                            self.lblName.text = "name is \(name)"
                        }
                        if let size = content.value(forKey: "size") as? NSNumber{
                            self.lblSize.text = " Size is \(size.stringValue)"
                        }
                        if let subject =  content.value(forKey: "subject") as? String{
                            print(subject)
                            if subject == ""{
                    self.btnAssignStudents.isHidden = true
                            }
                         
                        }
                        self.view.dismissProgress()
                    }
                }
                break;
            case.failure(let error):
                print(error)
                break
            }
        }
    }
}

extension ClassromDetailViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return subjects.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subjects[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtSubject.text = subjects[row].name
        subjectID = subjects[row].id!
    }
}

