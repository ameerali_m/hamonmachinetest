//
//  ViewController.swift
//  ameeralimk
//
//  Created by ameerali on 23/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func goToClassrooms(_ sender: Any) {
        self.performSegue(withIdentifier: "classroom", sender: self)

    }
    @IBAction func goToStudents(_ sender: Any) {
        self.performSegue(withIdentifier: "students", sender: self)
    }
    
    @IBAction func goToSubjects(_ sender: Any) {
        self.performSegue(withIdentifier: "subjects", sender: self)

    }
}

