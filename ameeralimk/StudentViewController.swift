//
//  StudentViewController.swift
//  ameeralimk
//
//  Created by ameerali on 23/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import UIKit

class StudentViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var studentData = [Students]()
    var selectedIndex = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
       getallStudents()

    }
    func getallStudents(){
        
        
        ApiHelper.get(url: BGNetworkConstants.STUDENTS) { (response) in
            
            switch(response.result){
            case .success(_):
                let res = response.result.value as? NSDictionary
                let data = res?.value(forKey: "students") as? NSArray
                let count = data?.count
                if count != nil{
                    for i in 0...count! - 1 {
                        
                        let dic = data?.object(at: i) as? NSDictionary
            self.studentData.append(Students.init(dic: dic!))
                   
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
             
                 break
            case .failure(_):
                print("error")
                break
            }        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dest = segue.destination as? StudentDetailViewController{
            dest.id = studentData[selectedIndex].id!
        }
    }
}
extension StudentViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return studentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "student", for: indexPath) as? StudentCell
        cell?.lblName.text = studentData[indexPath.row].name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "StudentDetail", sender: self)
    }
    
    
    
}
