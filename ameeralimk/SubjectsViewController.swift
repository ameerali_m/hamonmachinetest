//
//  SubjectsViewController.swift
//  ameeralimk
//
//  Created by ameerali on 23/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import UIKit

class SubjectsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var subjectData = [Subjects]()
    var selectedIndex = Int()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getAllSubjects()
    }
    func getAllSubjects(){
        ApiHelper.get(url: BGNetworkConstants.SUBJECTS) { (response) in
            
            switch(response.result){
            case .success(_):
                let res = response.result.value as? NSDictionary
                let data = res?.value(forKey: "subjects") as? NSArray
                let count = data?.count
                if count != nil{
                    for i in 0...count! - 1 {
                        let dic = data?.object(at: i) as? NSDictionary
                        self.subjectData.append(Subjects.init(dic: dic!))
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                break
            case .failure(_):
                print("error")
                break
            }        }
        

    }
    
 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if let destination  = segue.destination as? SubjectDetViewController{
            
            destination.id = selectedIndex
        }
       
    }
    

}
extension SubjectsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return subjectData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "subjects", for: indexPath) as? SubjectCell
        cell!.lblSubject.text = subjectData[indexPath.row].name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "subjectDet", sender: self)
    }
    
    
    
}
