//
//  AssignStudentViewController.swift
//  ameeralimk
//
//  Created by ameerali on 25/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import UIKit

class AssignStudentViewController: UIViewController {
    @IBOutlet weak var txtStudents: UITextField!
    
    var studentData = [Students]()
    var studentID = Int()
    var picker = UIPickerView()
    var Subid = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        txtStudents.inputView = picker
        getAllStudent()
        // Do any additional setup after loading the view.
    }
    func getAllStudent(){
        ApiHelper.get(url: BGNetworkConstants.STUDENTS) { (response) in
            
            switch(response.result){
            case .success(_):
                let res = response.result.value as? NSDictionary
                let data = res?.value(forKey: "students") as? NSArray
                let count = data?.count
                if count != nil{
                    for i in 0...count! - 1 {
                        let dic = data?.object(at: i) as? NSDictionary
                        self.studentData.append(Students.init(dic: dic!))
                    }
                }
                DispatchQueue.main.async {
                    print(self.studentData)
                    self.picker.reloadAllComponents()
                }
                break
            case .failure(let error):
                print(error)
                break
            }        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func submit(_ sender: Any) {
        
     
        
        let student = "student=\(String(studentID))"
        let subject = "&subject=\(String(Subid))"
      
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept-Encoding": "gzip, deflate",
        ]
        
        let postData = NSMutableData(data: student.data(using: String.Encoding.utf8)!)
        postData.append(subject.data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://hamon-interviewapi.herokuapp.com/registration/?api_key=EbDd6")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                DispatchQueue.main.async {
                    
                    Utils.showAlert(title: "student", message: "student assigned succesfully", delegate: self)
                }
                let httpResponse = response as? HTTPURLResponse
            
                print(response)
            }
        })
        
        dataTask.resume()
    }
    
}
extension AssignStudentViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
return 1    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return studentData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return studentData[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtStudents.text = studentData[row].name
        studentID = studentData[row].id!
    }
    
    
    
}
