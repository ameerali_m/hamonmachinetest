//
//  ClassroomViewController.swift
//  ameeralimk
//
//  Created by ameerali on 23/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import UIKit

class ClassroomViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var classroomData = [Classroom]()
    var selectedIndex = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        getAllClassrooms()
        // Do any additional setup after loading the view.
    }
    
    func getAllClassrooms(){
        ApiHelper.get(url: BGNetworkConstants.CLASSROOMS) { (response) in
            
            switch(response.result){
            case .success(_):
                print(response)
                let res = response.result.value as? NSDictionary
                let data = res?.value(forKey: "classrooms") as? NSArray
                let count = data?.count
                if count != nil{
                    for i in 0...count! - 1 {
                        let dic = data?.object(at: i) as? NSDictionary
                        self.classroomData.append(Classroom.init(dic: dic!))
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                break
            case .failure(_):
                print("error")
                break
            }        }
        
        
    }
    
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if let destination = segue.destination as? ClassromDetailViewController{
            
            destination.id = selectedIndex
        }
        
        
    }

}

extension ClassroomViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return classroomData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "classroom", for: indexPath) as? ClassroomCell
        
        cell?.lblClassroom.text = classroomData[indexPath.row].name
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "classroomDet", sender: self)
    }
    
    
    
}
