//
//  StudentDetailViewController.swift
//  ameeralimk
//
//  Created by ameerali on 24/11/19.
//  Copyright © 2019 ameerali. All rights reserved.
//

import UIKit
import iProgressHUD
class StudentDetailViewController: UIViewController {
    
    
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var lblAge:UILabel!
    
    
    var id = Int()
    
    var picker = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        let iprogress: iProgressHUD = iProgressHUD()
        iprogress.isShowModal = false
        iprogress.boxSize =  25
        iprogress.boxColor = UIColor.darkGray
        
        //iprogress.captionColor = UIColor.white
        iprogress.indicatorColor = UIColor.white
        iprogress.isShowCaption = false
        iprogress.isTouchDismiss = true
        iprogress.indicatorStyle = .ballSpinFadeLoader
        iprogress.alphaModal = 1
        iprogress.attachProgress(toViews: view)
        getDetailByID()
    }
    
    func getDetailByID(){
        view.showProgress()
        let url = "https://hamon-interviewapi.herokuapp.com/students/\(id)?api_key=EbDd6"
        ApiHelper.get(url: url) { (response) in
            switch(response.result){
            case.success(let data):
                print(data)
                if let content = data as? NSDictionary{
                    DispatchQueue.main.async {
                        if let age = content.value(forKey: "age") as? NSNumber {
                            self.lblAge.text = " age is \(age.stringValue)"
                        }
                        if let name = content.value(forKey: "name") as? String{
                             self.lblName.text = "name is \(name)"
                        }
                        if let mail = content.value(forKey: "email") as? String{
                              self.lblEmail.text = " Email is \(mail)"
                        }
                        
                        self.view.dismissProgress()
                    }
                }
                break;
            case.failure(let error):
                print(error)
                break
        }
    }
}
    }




