//
//  ApiHelper.swift
//  B&G Delivery
//
//  Created by Sherin S on 1/20/19.
//  Copyright © 2019 Brown and Greens Pty Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner

class ApiHelper: NSObject {
    typealias CompletionHandlerResponse = (_ Response:DataResponse<Any>) -> Void
    class func get(url:String,completionBlock: @escaping (DataResponse<Any>) -> Void)
    {
        //SwiftSpinner.show("Loading")
       // SwiftSpinner.setTitleFont(UIFont.init(name: "GTWalsheimLight", size: 15))
        
        let authKey = "Basic \(Constants.NetworkHeaderKey)"
       


        let headersValue: HTTPHeaders = [
            "Authorization": authKey,
            "Content-Type": "application/json"
        ]
        AF.request(url, headers: headersValue).responseJSON { (response) in
            completionBlock(response)
            SwiftSpinner.hide()
        }
    }
    
    class func post(url:String, parameter:[String:String], completionBlock: @escaping (DataResponse<Any>) -> Void)
    {
       // SwiftSpinner.show("Loading")
      //  SwiftSpinner.setTitleFont(UIFont.init(name: "GTWalsheimLight", size: 15))
        

        let authKey = "Basic \(Constants.NetworkHeaderKey)"
        let headersValue: HTTPHeaders = [
            "Authorization": authKey,
            "Content-Type": "application/json"
        ]
        
        AF.request(url, method: .post, parameters: parameter,encoding: JSONEncoding.default, headers: headersValue).responseJSON {
            response in
            completionBlock(response)
        //    SwiftSpinner.hide()

        }
    }
    
    class func patch(url:String, parameter:[String:String], completionBlock: @escaping (DataResponse<Any>) -> Void)
    {
        // SwiftSpinner.show("Loading")
        //  SwiftSpinner.setTitleFont(UIFont.init(name: "GTWalsheimLight", size: 15))
        
        
        let authKey = "Basic \(Constants.NetworkHeaderKey)"
        let headersValue: HTTPHeaders = [
            "Authorization": authKey,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        

        AF.request(url,method:.patch, parameters: parameter,encoding: URLEncoding.httpBody, headers: headersValue).responseJSON {
            response in
            completionBlock(response)
            //    SwiftSpinner.hide()
        }
    }
    
}
