//
//  BGNetworkConstants.swift
//  B&G Delivery
//
//  Created by Ojas Chimane on 15/1/19.
//  Copyright © 2019 Brown and Greens Pty Ltd. All rights reserved.
//

import Foundation

class BGNetworkConstants {
    
    static let S_BASE_URL   =   "https://hamon-interviewapi.herokuapp.com/"
    static let P_BASE_URL   =   "https://hamon-interviewapi.herokuapp.com/"
    
    let apikey = "?api_key=EbDd6"
    // Change the flag according to the environment
    static let isStaging    =   false
    
    static let BASE_URL     =   isStaging ? S_BASE_URL : P_BASE_URL
    
    static let LOGIN_URL    =   BASE_URL + ""
    static let STUDENTS    =   BASE_URL + "students/?api_key=EbDd6"
    static let SUBJECTS    =   BASE_URL + "subjects/?api_key=EbDd6"
    static let CLASSROOMS    =   BASE_URL + "classrooms/?api_key=EbDd6"

    
}
