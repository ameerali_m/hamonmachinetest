//
//  Utils.swift
//  B&G Delivery
//
//  Created by Sherin S on 1/21/19.
//  Copyright © 2019 Brown and Greens Pty Ltd. All rights reserved.
//

import UIKit

class Utils: NSObject {
    class func showAlert(title:String, message:String,delegate:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        delegate.present(alert, animated: true, completion: nil)

    }

}
