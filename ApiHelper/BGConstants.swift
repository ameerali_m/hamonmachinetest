//
//  BGConstants.swift
//  B&G Delivery
//
//  Created by Ojas Chimane on 12/1/19.
//  Copyright © 2019 Brown and Greens Pty Ltd. All rights reserved.
//
import UIKit

struct Constants {
    
    static let NetworkHeaderKey    =   "YjduZzp1VDcja001MUpmbldhNl5l"
    
    
    struct Api {
        
    }
    
    struct CellIdentifiers {
        static let OrderCellsIdentifier         =   "OrderCellsIdentifier"
        static let OrderDetailCellIdentifier    =   "OrderDetailCellIdentifier"
        static let ConfirmProductCellIdentifier =   "ConfirmProductCellIdentifier"

    }
    
    struct SegueIdentifiers {
     //   static let Master = "MasterViewController"
     //   static let Detail = "DetailViewController"
    }
    
    struct Color {
        static let Bgreen = UIColor(red: 131/255.0, green: 187/255.0, blue: 53/255.0, alpha: 1.0)
    }
}
