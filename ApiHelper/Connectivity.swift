//
//  Connectivity.swift
//  B&G Delivery
//
//  Created by Ojas Chimane on 14/1/19.
//  Copyright © 2019 Brown and Greens Pty Ltd. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
